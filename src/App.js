import React from 'react';
// import {Header} from './components/header/Header';
import {GlobalStyle} from './GlobalStyles';
import {BodyWrap} from './components/bodyWrap/BodyWrap';
import {Photos} from './components/photos/Photos';
import ValueRoute from './contexts/ValueContext';


function App() {
  return (
    <>
    <GlobalStyle/>
      <BodyWrap>
        {/* <Header/> */}
      <ValueRoute>
        <Photos/>
      </ValueRoute>
      </BodyWrap>
    </>
  );
}

export default App;
