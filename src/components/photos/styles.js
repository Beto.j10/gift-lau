import styled, { keyframes } from 'styled-components';
import { ValueContext } from '../../contexts/ValueContext';
import React, { useState } from 'react';


const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

export const PhotoWraper = styled.div`
    position: relative;
    margin: 0 0;
    padding: 0 0;
    display: flex;
    flex-flow: wrap;
    justify-content: center;
    /* height: fit-content;
    width: fit-content; */
    /* width: 100%; */
`

// const {setValueRoutes} = useContext(ValueContext)

// const [valuePos, setValuePos] = useState("")

export const StylePhotos = styled.img`
    position:relative;
    border-radius:50%;
    animation: ${rotate360} infinite 50s linear;
    width: 5em;
    height: 5em;
    object-fit:cover;
    margin: 0 0;
    padding: 0 0;
    box-shadow: 1px 1px 3px 1px rgba(0,0,0,.6);
    opacity: 0;
    animation: opac 2s infinite  linear 1s alternate-reverse;
    @keyframes opac {
      from{opacity: .2;}
      to{opacity: .8;}
    }
  &:hover {
      border-radius:35%;
      position: relative;
      animation: ${rotate360} infinite 0s linear;
      z-index:1000 ;
      opacity: 1;
      @media (orientation: landscape) {
        /* height: 5em;
        width: 5em; */
        /* left: 50vw;
        top: 15vh; */
      }
      @media (orientation: portrait) {
        /* height: 5em;
        width: 5em;
        */
      } 
        
  }
`
export const StylePhotosFocus = styled.img`
    /* display: block; */
    position: fixed;
    border-radius:10%;
    object-fit:cover;
    margin: 0 0;
    padding: 0 0;
    z-index:1000 ;
    box-shadow: 0 0 20px 2px rgba(232,99,146,.6);
    /* top: 70v; */
      @media (orientation: landscape) {
        height: 70vh;
        width: 70vh;
        /* left: 2vw; */
        right: 3vw;
        /* top: 2vh; */
        bottom: 1vh;
      }
      @media (orientation: portrait) {
        height: 70vw;
        width: 70vw;
        left: 3vw;
        bottom: 1vh;
      }
` 