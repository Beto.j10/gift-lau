import React,{useState} from 'react';
import {StylePhotos, PhotoWraper, StylePhotosFocus} from './styles';
import ReactHover from 'react-hover'

const optionsCursorTrueWithMargin = {
    followCursor: false,
    shiftX: 0,
    shiftY: 0,
 }

 export const Photos = () =>{
    const [showImage, setShowImage] = useState(0)
    const numeroAleatorio = (min, max) => {
        var num = Math.floor(Math.random() * (1 + max - min) + min);
        setShowImage(num)
      }
    
    const photoArray = [];

    const fillRoute = () =>{
        for (let i = 1; i < 192; i++) {
            var image = `../../assets/static/${i}.jpg`
            photoArray.push(image);
        }
    }
    fillRoute()
    console.log(photoArray)
    return(
        
        <>

                <PhotoWraper>
                    {photoArray.map(data =>{
                        return(
                            <>
                            <ReactHover options={optionsCursorTrueWithMargin}>
                            <ReactHover.Trigger type='trigger'>
                                <StylePhotos  src={data} alt='photo' route={data} />
                            </ReactHover.Trigger>
                            <ReactHover.Hover type='hover'>
                                <StylePhotosFocus  src={data} alt='photo' route={showImage} />
                            </ReactHover.Hover>
                            </ReactHover>

                            </>
                        )
                    })}
                </PhotoWraper>

        </>
    )
}