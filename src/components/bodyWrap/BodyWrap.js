import React from 'react';
import {BodyStyle} from './styles';

export const BodyWrap = ({children}) =>{

    return(
        <BodyStyle>
            {children}
        </BodyStyle>
    )
}