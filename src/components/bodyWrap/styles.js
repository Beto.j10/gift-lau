import React from 'react';

import styled from 'styled-components';

export const BodyStyle = styled.div`
    
    top: 1em;
    position:relative;
    background: #E3E3E3;
    /* background: #EEC9BD; */
    /* height: 100vh; */
    margin: 0 auto;
    max-width: 95vw;
    /* box-shadow: 0 0 5px rgba(0, 0, 0, .8); */
    box-shadow: 0 0 30px 2px rgba(232,99,146,.7);
    width: 100%;
    border-radius:5em;
    padding:2em 2em;
    font-size: 16px;
    overflow:hidden;
    @media screen and (max-width: 767px){
        font-size: .8em;
    }
    @media (orientation: landscape) {
        margin: 0 auto 70vh ;
      }
      @media (orientation: portrait) {
        margin: 0 auto 70vw;
      }
`