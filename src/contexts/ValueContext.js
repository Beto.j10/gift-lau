import React,{createContext,useState} from 'react';

export const ValueContext = createContext({
    valueRoute:"",
    setValueRoute:() => null,
})

const ValueRoute = ({children}) =>{

    const [valueRoute, setValueRoute] = useState("")

    return(
        <ValueContext.Provider value={{valueRoute, setValueRoute}}>
            {children}
        </ValueContext.Provider>
    )

}
export default ValueRoute